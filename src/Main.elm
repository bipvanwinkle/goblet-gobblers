module Main exposing (..)

import Html exposing (Html, text, div, img, button)
import Html.Attributes exposing (src, style, id)
import DnD
import Debug


---- MODEL ----


type alias Draggable =
    { pastPosition : GamePosition
    , gobbler : Gobbler
    }


type Team
    = One
    | Two


type Size
    = Small
    | Medium
    | Large


type alias Placement =
    { position : Position
    , team : Team
    , size : Size
    }


type Gobbler
    = Gobbler Size Team


type GamePosition
    = OnBoard Position
    | OffBoard Team


type Position
    = TopLeft
    | TopMiddle
    | TopRight
    | CenterLeft
    | CenterMiddle
    | CenterRight
    | BottomLeft
    | BottomMiddle
    | BottomRight


type alias Board =
    { topLeft : List Gobbler
    , topMiddle : List Gobbler
    , topRight : List Gobbler
    , centerLeft : List Gobbler
    , centerMiddle : List Gobbler
    , centerRight : List Gobbler
    , bottomLeft : List Gobbler
    , bottomMiddle : List Gobbler
    , bottomRight : List Gobbler
    }


emptyBoard : Board
emptyBoard =
    { topLeft = []
    , topMiddle = []
    , topRight = []
    , centerLeft = []
    , centerMiddle = []
    , centerRight = []
    , bottomLeft = []
    , bottomMiddle = []
    , bottomRight = []
    }


type alias PieceCount =
    { small : Int
    , medium : Int
    , large : Int
    }


type alias Model =
    { board : Board
    , currentTurn : Team
    , draggable : DnD.Draggable Position Draggable
    , teamOnePieces : PieceCount
    , teamTwoPieces : PieceCount
    , winner : Maybe Team
    }


isJust : Maybe a -> Bool
isJust maybe =
    case maybe of
        Nothing ->
            False

        Just _ ->
            True


liftMaybe : List (Maybe a) -> Maybe (List a)
liftMaybe maybes =
    List.foldr
        (\maybe mList ->
            case mList of
                Nothing ->
                    Nothing

                Just list ->
                    case maybe of
                        Nothing ->
                            Nothing

                        Just just ->
                            Just (just :: list)
        )
        (Just [])
        maybes


threeInARow : List (List Gobbler) -> Maybe Team
threeInARow gobblerLists =
    let
        heads =
            List.map List.head gobblerLists

        allOccupied =
            List.all isJust heads

        justs =
            Maybe.withDefault [] (liftMaybe heads)

        allOne =
            List.all (\(Gobbler _ team) -> isSameTeam team One) justs

        allTwo =
            List.all (\(Gobbler _ team) -> isSameTeam team Two) justs

        _ =
            Debug.log "" ( justs, allOccupied, allOne, allTwo )
    in
        if allOccupied then
            if allOne then
                Just One
            else if allTwo then
                Just Two
            else
                Nothing
        else
            Nothing


firstJust : List (Maybe a) -> Maybe a
firstJust maybes =
    List.foldr
        (\maybe acc ->
            case acc of
                Nothing ->
                    maybe

                Just _ ->
                    acc
        )
        Nothing
        maybes


isGameOver : Board -> Maybe Team
isGameOver board =
    firstJust
        [ threeInARow [ board.topLeft, board.topMiddle, board.topRight ]
        , threeInARow [ board.centerLeft, board.centerMiddle, board.centerRight ]
        , threeInARow [ board.bottomLeft, board.bottomMiddle, board.bottomRight ]
        , threeInARow [ board.topLeft, board.centerMiddle, board.bottomRight ]
        , threeInARow [ board.topRight, board.centerMiddle, board.bottomLeft ]
        , threeInARow [ board.topLeft, board.centerLeft, board.bottomLeft ]
        , threeInARow [ board.topMiddle, board.centerMiddle, board.bottomMiddle ]
        , threeInARow [ board.topRight, board.centerRight, board.bottomRight ]
        ]


isSameTeam : Team -> Team -> Bool
isSameTeam left right =
    case ( left, right ) of
        ( One, One ) ->
            True

        ( Two, Two ) ->
            True

        ( _, _ ) ->
            False


popList : List a -> List a
popList list =
    case list of
        [] ->
            []

        first :: rest ->
            rest


dnd =
    DnD.init DnDMsg Drop


init : ( Model, Cmd Msg )
init =
    ( { board = emptyBoard
      , currentTurn = One
      , draggable = dnd.model
      , teamOnePieces =
            { small = 2
            , medium = 2
            , large = 2
            }
      , teamTwoPieces =
            { small = 2
            , medium = 2
            , large = 2
            }
      , winner = Nothing
      }
    , Cmd.none
    )


removePiece : Draggable -> Board -> Board
removePiece { gobbler, pastPosition } board =
    case pastPosition of
        OnBoard position ->
            case position of
                TopLeft ->
                    { board | topLeft = popList board.topLeft }

                TopMiddle ->
                    { board | topMiddle = popList board.topMiddle }

                TopRight ->
                    { board | topRight = popList board.topRight }

                CenterLeft ->
                    { board | centerLeft = popList board.centerLeft }

                CenterMiddle ->
                    { board | centerMiddle = popList board.centerMiddle }

                CenterRight ->
                    { board | centerRight = popList board.centerRight }

                BottomLeft ->
                    { board | bottomLeft = popList board.bottomLeft }

                BottomMiddle ->
                    { board | bottomMiddle = popList board.bottomMiddle }

                BottomRight ->
                    { board | bottomRight = popList board.bottomRight }

        OffBoard _ ->
            board


placePiece : Board -> Placement -> Board
placePiece board placement =
    case placement.position of
        TopLeft ->
            { board | topLeft = (Gobbler placement.size placement.team) :: board.topLeft }

        TopMiddle ->
            { board | topMiddle = (Gobbler placement.size placement.team) :: board.topMiddle }

        TopRight ->
            { board | topRight = (Gobbler placement.size placement.team) :: board.topRight }

        CenterLeft ->
            { board | centerLeft = (Gobbler placement.size placement.team) :: board.centerLeft }

        CenterMiddle ->
            { board | centerMiddle = (Gobbler placement.size placement.team) :: board.centerMiddle }

        CenterRight ->
            { board | centerRight = (Gobbler placement.size placement.team) :: board.centerRight }

        BottomLeft ->
            { board | bottomLeft = (Gobbler placement.size placement.team) :: board.bottomLeft }

        BottomMiddle ->
            { board | bottomMiddle = (Gobbler placement.size placement.team) :: board.bottomMiddle }

        BottomRight ->
            { board | bottomRight = (Gobbler placement.size placement.team) :: board.bottomRight }


isGreaterSize : Size -> Size -> Bool
isGreaterSize left right =
    case ( left, right ) of
        ( Large, Medium ) ->
            True

        ( Large, Small ) ->
            True

        ( Medium, Small ) ->
            True

        ( _, _ ) ->
            False


retrieveCell : Board -> Position -> List Gobbler
retrieveCell board position =
    case position of
        TopLeft ->
            board.topLeft

        TopMiddle ->
            board.topMiddle

        TopRight ->
            board.topRight

        CenterLeft ->
            board.centerLeft

        CenterMiddle ->
            board.centerMiddle

        CenterRight ->
            board.centerRight

        BottomLeft ->
            board.bottomLeft

        BottomMiddle ->
            board.bottomMiddle

        BottomRight ->
            board.bottomRight



---- UPDATE ----


type Msg
    = NoOp
    | Drop Position Draggable
    | DnDMsg (DnD.Msg Position Draggable)


isValidMove : Board -> Gobbler -> Position -> Bool
isValidMove board (Gobbler size _) position =
    let
        cell =
            retrieveCell board position

        isValid =
            case cell of
                [] ->
                    True

                (Gobbler cellSize _) :: _ ->
                    isGreaterSize size cellSize
    in
        isValid


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Drop position draggable ->
            let
                { pastPosition, gobbler } =
                    draggable

                (Gobbler size team) =
                    gobbler

                onePieceCount =
                    model.teamOnePieces

                twoPieceCount =
                    model.teamTwoPieces

                decCount =
                    case pastPosition of
                        OffBoard _ ->
                            1

                        OnBoard _ ->
                            0

                ( onePieces, twoPieces ) =
                    case team of
                        One ->
                            case size of
                                Large ->
                                    ( { onePieceCount | large = onePieceCount.large - decCount }
                                    , twoPieceCount
                                    )

                                Medium ->
                                    ( { onePieceCount | medium = onePieceCount.medium - decCount }
                                    , twoPieceCount
                                    )

                                Small ->
                                    ( { onePieceCount | small = onePieceCount.small - decCount }
                                    , twoPieceCount
                                    )

                        Two ->
                            case size of
                                Large ->
                                    ( onePieceCount
                                    , { twoPieceCount | large = twoPieceCount.large - decCount }
                                    )

                                Medium ->
                                    ( onePieceCount
                                    , { twoPieceCount | medium = twoPieceCount.medium - decCount }
                                    )

                                Small ->
                                    ( onePieceCount
                                    , { twoPieceCount | small = twoPieceCount.small - decCount }
                                    )
            in
                if isValidMove model.board (Gobbler size team) position then
                    let
                        updatedModel =
                            { model
                                | board =
                                    removePiece draggable
                                        (placePiece model.board
                                            { position = position
                                            , size = size
                                            , team = team
                                            }
                                        )
                                , currentTurn =
                                    case model.currentTurn of
                                        One ->
                                            Two

                                        Two ->
                                            One
                                , teamOnePieces = onePieces
                                , teamTwoPieces = twoPieces
                            }

                        nowUpdateGameStatus =
                            { updatedModel | winner = isGameOver updatedModel.board }
                    in
                        ( nowUpdateGameStatus, Cmd.none )
                else
                    ( model, Cmd.none )

        DnDMsg msg ->
            ( { model
                | draggable = DnD.update msg model.draggable
              }
            , Cmd.none
            )



---- VIEW ----


viewSize : Size -> String
viewSize size =
    case size of
        Small ->
            "Small"

        Medium ->
            "Medium"

        Large ->
            "Large"


viewTeam : Team -> String
viewTeam team =
    case team of
        One ->
            "One"

        Two ->
            "Two"


cellId : Team -> Position -> String
cellId team position =
    case ( team, nonRightCell position ) of
        ( One, True ) ->
            "non-right-one-cell"

        ( One, False ) ->
            "one-cell"

        ( Two, True ) ->
            "non-right-two-cell"

        ( Two, False ) ->
            "two-cell"


nonRightCell : Position -> Bool
nonRightCell position =
    case position of
        TopLeft ->
            True

        TopMiddle ->
            True

        CenterLeft ->
            True

        CenterMiddle ->
            True

        BottomLeft ->
            True

        BottomMiddle ->
            True

        _ ->
            False


viewSingleGobbler : Gobbler -> Html Msg
viewSingleGobbler (Gobbler size team) =
    let
        avatarUrl =
            case team of
                One ->
                    "./DinoSprites_doux.gif"

                Two ->
                    "./DinoSprites_vita.gif"

        containerClass =
            case size of
                Small ->
                    "small-gobbler-container"

                Medium ->
                    "medium-gobbler-container"

                Large ->
                    "large-gobbler-container"

        attributes =
            [ id containerClass ]

        children =
            [ img [ src avatarUrl, id "gobbler" ] [] ]
    in
        div attributes children


viewCell : Team -> List Gobbler -> Placement -> Html Msg
viewCell currentTurn mGobbler placement =
    case mGobbler of
        [] ->
            dnd.droppable
                placement.position
                [ if nonRightCell placement.position then
                    id "non-right-empty-cell"
                  else
                    id "empty-cell"
                ]
                []

        (Gobbler size team) :: _ ->
            if isSameTeam team currentTurn then
                dnd.draggable
                    { gobbler = (Gobbler size team)
                    , pastPosition = OnBoard placement.position
                    }
                    [ id (cellId team placement.position) ]
                    [ (dnd.droppable
                        placement.position
                        []
                        [ viewGobbler (Gobbler size team) ]
                      )
                    ]
            else
                dnd.droppable
                    placement.position
                    [ id (cellId team placement.position) ]
                    [ viewGobbler (Gobbler size team) ]


viewBoard : Model -> Html Msg
viewBoard { board, currentTurn } =
    div [ id "board" ]
        [ div [ id "top-row" ]
            [ viewCell
                currentTurn
                board.topLeft
                { team = currentTurn, size = Large, position = TopLeft }
            , viewCell
                currentTurn
                board.topMiddle
                { team = currentTurn, size = Large, position = TopMiddle }
            , viewCell
                currentTurn
                board.topRight
                { team = currentTurn, size = Large, position = TopRight }
            ]
        , div [ id "top-row" ]
            [ viewCell
                currentTurn
                board.centerLeft
                { team = currentTurn, size = Large, position = CenterLeft }
            , viewCell
                currentTurn
                board.centerMiddle
                { team = currentTurn, size = Large, position = CenterMiddle }
            , viewCell
                currentTurn
                board.centerRight
                { team = currentTurn, size = Large, position = CenterRight }
            ]
        , div [ id "board-row" ]
            [ viewCell
                currentTurn
                board.bottomLeft
                { team = currentTurn, size = Large, position = BottomLeft }
            , viewCell
                currentTurn
                board.bottomMiddle
                { team = currentTurn, size = Large, position = BottomMiddle }
            , viewCell
                currentTurn
                board.bottomRight
                { team = currentTurn, size = Large, position = BottomRight }
            ]
        ]


viewGobbler : Gobbler -> Html Msg
viewGobbler (Gobbler size team) =
    let
        avatarUrl =
            case team of
                One ->
                    "./DinoSprites_doux.gif"

                Two ->
                    "./DinoSprites_vita.gif"

        containerClass =
            case size of
                Small ->
                    "small-gobbler-container"

                Medium ->
                    "medium-gobbler-container"

                Large ->
                    "large-gobbler-container"

        attributes =
            [ id containerClass ]

        children =
            [ img [ src avatarUrl, id "gobbler" ] [] ]
    in
        div
            attributes
            children


viewSideGobbler : Team -> Gobbler -> Html Msg
viewSideGobbler currentTurn (Gobbler size team) =
    if currentTurn == team then
        dnd.draggable
            { gobbler = (Gobbler size team)
            , pastPosition = OffBoard team
            }
            []
            [ viewGobbler (Gobbler size team) ]
    else
        div
            []
            [ viewGobbler (Gobbler size team) ]


viewGobblers : Int -> Team -> Gobbler -> List (Html Msg)
viewGobblers count currentTurn gobbler =
    if count == 0 then
        []
    else
        viewSideGobbler currentTurn gobbler :: (viewGobblers (count - 1) currentTurn gobbler)


viewPieces : Team -> Team -> PieceCount -> Html Msg
viewPieces currentTurn team pieceCount =
    div []
        [ div []
            [ div
                [ id "gobbler-row" ]
                (viewGobblers
                    pieceCount.large
                    currentTurn
                    (Gobbler Large team)
                )
            , div
                [ id "gobbler-row" ]
                (viewGobblers
                    pieceCount.medium
                    currentTurn
                    (Gobbler Medium team)
                )
            , div
                [ id "gobbler-row" ]
                (viewGobblers
                    pieceCount.small
                    currentTurn
                    (Gobbler Small team)
                )
            ]
        ]


viewHeader : Html Msg
viewHeader =
    div
        []
        [ text "Gobblet Gobblers" ]


view : Model -> Html Msg
view model =
    case model.winner of
        Nothing ->
            div []
                [ viewHeader
                , div [ id "container" ]
                    [ viewPieces model.currentTurn One model.teamOnePieces
                    , viewBoard model
                    , viewPieces model.currentTurn Two model.teamTwoPieces
                    , DnD.dragged model.draggable (\{ gobbler } -> viewSingleGobbler gobbler)
                    ]
                ]

        Just winner ->
            div [] [ text (toString winner) ]



---- PROGRAM ----


subscriptions : Model -> Sub Msg
subscriptions model =
    dnd.subscriptions model.draggable


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
